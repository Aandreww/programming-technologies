#include <string.h>
#include <ctype.h>

double numberOfDigits(char* str) {
    int n = 0;
    for (int i = 0; i < strlen(str); i++)
        if (isdigit(str[i]))
            n++;
    return n;
}
