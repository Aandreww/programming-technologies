# gcc -shared -o lib.dll -fPIC lib.c
# gcc -shared -o lib.so -fPIC lib.c
import ctypes

lib = ctypes.CDLL('./lib.dll')
lib.numberOfDigits.restype = ctypes.c_double
lib.numberOfDigits.argtypes = (ctypes.c_char_p, )

if __name__ == "__main__":
    cycle = True
    while cycle:
        # sting = b"1 Mississippi, 2 Mississippi, 3 Mississippi, 4 Mississippi, 5 Mississippi, 6 Mississippi, 7 Mississippi, 8 Mississippi"
        string = input("Enter your string: ")
        x = lib.numberOfDigits(string.encode('utf-8'))
        print("Result: " + str(int(x)))
        choice = input("Wanna more?(Y/N)")
        if choice == "Y" or choice == "y":
            cycle = True
        else:
            cycle = False
    print("GoodBye!")
